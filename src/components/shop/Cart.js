import { useRecoilState, useRecoilValue } from "recoil";
import { cartState, ordersState, customersState } from "../../Router";
import axios from "axios";

export const Cart = () => {
  const [cart, setCart] = useRecoilState(cartState);
  const [orders, setOrders] = useRecoilState(ordersState);
  const customers = useRecoilValue(customersState);

  const deleteFromCart = (id) => {
    setCart({
      open: true,
      items: cart.items
        .map((item) =>
          item.product.id === id
            ? {
                product: item.product,
                qty: item.qty - 1,
              }
            : item
        )
        .filter((item) => item.qty > 0),
    });
  };

  const createOrder = (order) => {
    axios
      .post("http://localhost:8080/api/Orders", order)
      .then((response) => {
        setOrders([...orders, response.data]);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  return (
    <div className="drawer-side">
      <label
        for="my-drawer-4"
        className="drawer-overlay"
        onClick={() => {
          setCart({ ...cart, open: false });
        }}
      ></label>
      <ul className="menu p-4 overflow-y-auto w-80 bg-base-100 text-base-content">
        {cart.items.length > 0 ? (
          cart.items.map((item) => {
            return (
              <li className="menu-item">
                <div className="flex flex-col">
                  <img src={item.product.image} alt={item.product.name} />
                  <div className="flex w-full flex-row justify-around">
                    {item.qty > 1 ? (
                      <p className="text-base-content self-center">
                        {item.qty}x
                      </p>
                    ) : (
                      ""
                    )}
                    <p className="text-base-content self-center">
                      {item.product.name}
                    </p>
                    <p className="text-base-content self-center">
                      {item.product.price}€
                    </p>
                    <button
                      onClick={() => {
                        deleteFromCart(item.product.id);
                      }}
                      className="btn btn-error self-center btn-sm"
                    >
                      Supprimer
                    </button>
                  </div>
                </div>
              </li>
            );
          })
        ) : (
          <span className="text-center">Aucun produit dans le panier...</span>
        )}
        {cart.items.length > 0 ? (
          <li className="menu-item">
            <form
              className="mt-2 flex flex-col"
              onSubmit={(e) => {
                e.preventDefault();
                const { name, customer, reset } = e.target;
                createOrder({
                  name: name.value,
                  customer: customers[customer.value],
                  products: cart.items.map((item) => {
                    for (let i = 0; i < item.qty; i++) {
                      return item.product;
                    }
                  }),
                });
                reset();
              }}
            >
              <div className="form-group my-1">
                <label
                  className="block text-gray-700 text-sm font-bold mb-2"
                  htmlFor="name"
                >
                  Nom de la commande
                </label>
                <input
                  type="text"
                  id="name"
                  name="name"
                  placeholder="Nom de la commande"
                  className="input w-full max-w-xs border-2 border-black"
                ></input>
              </div>
              <div className="form-group my-1">
                <label
                  className="block text-gray-700 text-sm font-bold mb-2"
                  htmlFor="customer"
                >
                  Client
                </label>
                <select className="select w-full max-w-xs" name="customer">
                  <option disabled>Client</option>
                  {customers.map((customer, index) => {
                    return (
                      <option key={index} value={index}>
                        {customer.name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <div className="form-group my-3 text-center">
                <button
                  type="submit"
                  className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                >
                  Passer la commande
                </button>
              </div>
            </form>
          </li>
        ) : (
          ""
        )}
      </ul>
    </div>
  );
};
