import { useEffect } from "react";
import { useRecoilState } from "recoil";
import { cartState } from "../../Router";

export const Product = (props) => {
  const [cart, setCart] = useRecoilState(cartState);

  useEffect(() => {
    window.localStorage.setItem(
      "cart",
      JSON.stringify({ ...cart, open: false })
    );
  }, [cart]);

  const addToCart = (e) => {
    e.preventDefault();

    const isPresent = cart.items.find(
      (item) => item.product.id === props.product.id
    );

    if (isPresent) {
      setCart({
        open: true,
        items: cart.items.map((item) =>
          item.product.id === props.product.id
            ? {
                product: item.product,
                qty: item.qty + 1,
              }
            : item
        ),
      });
    } else {
      setCart({
        open: true,
        items: [...cart.items, { product: props.product, qty: 1 }],
      });
    }
  };

  return (
    <div className="card w-96 bg-base-100 shadow-xl">
      <figure>
        <img
          className="h-40 object-contain"
          src={props.product.image}
          alt={props.product.name}
        />
      </figure>
      <div className="card-body">
        <h2 className="card-title">{props.product.name}</h2>
        <p>{props.product.description}</p>
        <div className="card-actions justify-end">
          <button onClick={addToCart} className="btn btn-primary">
            {props.product.price}€
          </button>
        </div>
      </div>
    </div>
  );
};
