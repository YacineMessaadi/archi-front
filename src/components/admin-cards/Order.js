import axios from "axios";
import { useRecoilState } from "recoil";
import { ordersState } from "../../Router";

export const Order = (props) => {
  // axios post request to delete a order
  const remove = (order) => {
    axios
      .delete("http://localhost:8080/api/Orders/" + order.id)
      .then((response) => {
        setOrders(orders.filter((o) => o.id !== order.id));
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const [orders, setOrders] = useRecoilState(ordersState);
  const order = props.order;

  return (
    <div className="card w-96 bg-base-100 shadow-xl">
      <div className="card-body">
        <h2 className="card-title">{order.name}</h2>
        {order.products.map((product) => (
          <p key={product.id}>{product.name}</p>
        ))}
        <div className="card-actions justify-end">
          <div className="btn btn-disabled btn-primary">
            {order.customer.name}
          </div>
          <button onClick={() => remove(order)} className="btn btn-error">
            Supprimer
          </button>
        </div>
      </div>
    </div>
  );
};
