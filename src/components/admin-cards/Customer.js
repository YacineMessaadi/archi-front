import axios from "axios";
import { useRecoilState } from "recoil";
import { customersState } from "../../Router";

export const Customer = (props) => {
  // axios post request to delete a customer
  const remove = (customer) => {
    axios
      .delete("http://localhost:8080/api/Customers/" + customer.id)
      .then((response) => {
        setCustomers(customers.filter((p) => p.id !== customer.id));
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const [customers, setCustomers] = useRecoilState(customersState);
  const customer = props.customer;

  return (
    <div className="card w-96 bg-base-100 shadow-xl">
      <div className="card-body">
        <h2 className="card-title">{customer.name}</h2>
        {customer.orders.map((order) => (
          <p key={order.id}>Commande {order.id}</p>
        ))}
        <div className="card-actions justify-end">
          <button onClick={() => remove(customer)} className="btn btn-error">
            Supprimer
          </button>
        </div>
      </div>
    </div>
  );
};
