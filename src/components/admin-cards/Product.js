import axios from "axios";
import { useRecoilState } from "recoil";
import { productsState } from "../../Router";

export const Product = (props) => {
  // axios post request to delete a product
  const remove = (product) => {
    axios
      .delete("http://localhost:8080/api/Products/" + product.id)
      .then((response) => {
        setProducts(products.filter((p) => p.id !== product.id));
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const [products, setProducts] = useRecoilState(productsState);
  const product = props.product;

  return (
    <div className="card w-96 bg-base-100 shadow-xl">
      <figure>
        <img className="h-40 object-contain" src={product.image} alt="Shoes" />
      </figure>
      <div className="card-body">
        <h2 className="card-title">{product.name}</h2>
        <p>{product.description}</p>
        <div className="card-actions justify-end">
          <button className="btn btn-primary btn-disabled">
            {product.price}€
          </button>
          <button onClick={() => remove(product)} className="btn btn-error">
            Supprimer
          </button>
        </div>
      </div>
    </div>
  );
};
