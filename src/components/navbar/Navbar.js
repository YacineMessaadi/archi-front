import { Link } from "react-router-dom";
import { useRecoilState } from "recoil";
import { cartState } from "../../Router";

export const Navbar = () => {
  const [cart, setCart] = useRecoilState(cartState);

  return (
    <div className="navbar bg-base-100">
      <div className="flex-1">
        <a href="/" className="btn btn-ghost normal-case text-xl">
          IMT Shop
        </a>
      </div>
      <div className="flex-none">
        <ul className="menu menu-horizontal p-0 pr-2">
          <li>
            <button
              onClick={() => {
                setCart({ ...cart, open: true });
              }}
            >
              {cart.items.length > 0 ? (
                <div class=" flex items-center justify-center">
                  <div class="relative">
                    <div class="mr-1 text-center">Panier</div>
                    <div class="absolute top-0 right-0 -mr-1 -mt-1 w-2 h-2 rounded-full bg-primary animate-ping"></div>
                    <div class="absolute top-0 right-0 -mr-1 -mt-1 w-2 h-2 rounded-full bg-primary"></div>
                  </div>
                </div>
              ) : (
                "Panier"
              )}
            </button>
          </li>
          <li>
            <Link to="/admin">Admin</Link>
          </li>
        </ul>
      </div>
    </div>
  );
};
