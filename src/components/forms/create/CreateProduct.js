import axios from "axios";
import { useRecoilState } from "recoil";
import { productsState } from "../../../Router";

export const CreateProduct = () => {
  // axios post request to create a new product
  const createProduct = (product) => {
    axios
      .post("http://localhost:8080/api/Products", product)
      .then((response) => {
        setProducts([...products, response.data]);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const [products, setProducts] = useRecoilState(productsState);

  return (
    //a Tailwind form to create a new product that has a name, a description, a price and an image url
    <div className="w-2/3 flex flex-col items-center bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4">
      <h3 className="text-2xl mb-2">Créer un produit</h3>
      <form
        className="mt-2"
        onSubmit={(e) => {
          e.preventDefault();
          createProduct({
            name: e.target.name.value,
            description: e.target.description.value,
            price: e.target.price.value,
            image: e.target.image.value,
          });
          e.target.reset();
        }}
      >
        <div className="form-group my-1">
          <label
            className="block text-gray-700 text-sm font-bold mb-2"
            htmlFor="name"
          >
            Nom du produit
          </label>
          <input
            type="text"
            id="name"
            name="name"
            placeholder="Nom du produit"
            className="input w-full max-w-xs border-2 border-black"
          ></input>
        </div>
        <div className="form-group my-1">
          <label
            className="block text-gray-700 text-sm font-bold mb-2"
            htmlFor="description"
          >
            Description
          </label>
          <input
            type="text"
            id="description"
            name="description"
            placeholder="Description"
            className="input w-full max-w-xs border-2 border-black"
          ></input>
        </div>
        <div className="form-group my-1">
          <label
            className="block text-gray-700 text-sm font-bold mb-2"
            htmlFor="price"
          >
            Prix
          </label>
          <input
            type="text"
            id="price"
            name="price"
            placeholder="Prix"
            className="input w-full max-w-xs border-2 border-black"
          ></input>
        </div>
        <div className="form-group my-1">
          <label
            className="block text-gray-700 text-sm font-bold mb-2"
            htmlFor="image"
          >
            URL de l'image
          </label>
          <input
            type="text"
            id="image"
            name="image"
            placeholder="URL de l'image"
            className="input w-full max-w-xs border-2 border-black"
          ></input>
        </div>
        <div className="form-group my-3 text-center">
          <button
            type="submit"
            className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
          >
            Créer le produit
          </button>
        </div>
      </form>
    </div>
  );
};
