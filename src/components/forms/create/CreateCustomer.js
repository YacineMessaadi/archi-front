import axios from "axios";
import { useRecoilState } from "recoil";
import { customersState } from "../../../Router";

export const CreateCustomer = () => {
  // axios post request to create a new customer that has a name.
  const createCustomer = (customer) => {
    axios
      .post("http://localhost:8080/api/Customers", customer)
      .then((response) => {
        setCustomers([...customers, response.data]);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const [customers, setCustomers] = useRecoilState(customersState);

  return (
    //a Tailwind form to create a new customer that has a name
    <div className="w-2/3 flex flex-col items-center bg-white dark:bg shadow-md rounded px-8 pt-6 pb-8 mb-4">
      <h3 className="text-2xl mb-2">Créer un client</h3>
      <form
        className="mt-2"
        onSubmit={(e) => {
          e.preventDefault();
          createCustomer({
            name: e.target.name.value,
          });
          e.target.reset();
        }}
      >
        <div className="form-group my-1">
          <label
            className="block text-gray-700 text-sm font-bold mb-2"
            htmlFor="name"
          >
            Nom du client
          </label>
          <input
            type="text"
            id="name"
            name="name"
            placeholder="Nom du client"
            className="input w-full max-w-xs border-2 border-black"
          ></input>
        </div>
        <div className="form-group my-3 text-center">
          <button
            type="submit"
            className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
          >
            Créer le client
          </button>
        </div>
      </form>
    </div>
  );
};
