import axios from "axios";
import { useRecoilState, useRecoilValue } from "recoil";
import { customersState, ordersState } from "../../../Router";

export const CreateOrder = () => {
  // axios post request to create a new order that has a name and a customer id
  const createOrder = (order) => {
    axios
      .post("http://localhost:8080/api/Orders", order)
      .then((response) => {
        setOrders([...orders, response.data]);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const customers = useRecoilValue(customersState);
  const [orders, setOrders] = useRecoilState(ordersState);

  return (
    //a Tailwind form to create a new order that has a name and a customer id
    <div className="w-2/3 flex flex-col items-center bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4">
      <h3 className="text-2xl mb-2">Créer une commande</h3>
      <form
        className="mt-2"
        onSubmit={(e) => {
          e.preventDefault();
          const { name, customer, products, reset } = e.target;
          console.log(e.target.customer.value);
          createOrder({
            name: name.value,
            customer: customers[customer.value],
            products: products,
          });
          reset();
        }}
      >
        <div className="form-group my-1">
          <label
            className="block text-gray-700 text-sm font-bold mb-2"
            htmlFor="name"
          >
            Nom de la commande
          </label>
          <input
            type="text"
            id="name"
            name="name"
            placeholder="Nom de la commande"
            className="input w-full max-w-xs border-2 border-black"
          ></input>
        </div>
        <div className="form-group my-1">
          <label
            className="block text-gray-700 text-sm font-bold mb-2"
            htmlFor="customer"
          >
            Client
          </label>
          <select className="select w-full max-w-xs" name="customer">
            <option disabled>Client</option>
            {customers.map((customer, index) => {
              return (
                <option key={index} value={index}>
                  {customer.name}
                </option>
              );
            })}
          </select>
        </div>
        <div className="form-group my-3 text-center">
          <button
            type="submit"
            className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
          >
            Créer la commande
          </button>
        </div>
      </form>
    </div>
  );
};
