import axios from "axios";
import { useState } from "react";
import { useRecoilState, useRecoilValue } from "recoil";
import { customersState, ordersState } from "../../../Router";

export const ModifyOrder = () => {
  // axios post request to modify a order
  const modifyOrder = () => {
    console.log(selected);
    axios
      .put("http://localhost:8080/api/Orders/" + selected.id, selected)
      .then((response) => {
        setOrders(orders.map((o) => (o.id === selected.id ? selected : o)));
      })
      .catch((error) => {
        console.log(error);
      });
  };

  function handleChange(e) {
    setSelectedIndex(e.target.value);
    setSelected(orders[e.target.value]);
  }

  const [orders, setOrders] = useRecoilState(ordersState);
  const customers = useRecoilValue(customersState);

  const [selected, setSelected] = useState(orders[0]);
  const [selectedIndex, setSelectedIndex] = useState(0);

  return orders.length === 0 ? (
    <h2 className="text-xl text-warning my-5">Aucue commande à modifier 😕</h2>
  ) : (
    <div className="w-2/3 flex flex-col items-center bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4">
      <h3 className="text-2xl mb-2">Modifier une commande</h3>
      <form
        className="mt-2"
        onSubmit={(e) => {
          e.preventDefault();
          modifyOrder();
          e.target.reset();
        }}
      >
        <div className="form-group my-1">
          <label
            className="block text-gray-700 text-sm font-bold mb-2"
            htmlFor="order"
          >
            Commande à modifier
          </label>
          <select
            className="select w-full max-w-xs"
            value={selectedIndex}
            onChange={handleChange}
          >
            <option disabled>Commande à modifier</option>
            {orders.map((order, index) => {
              return (
                <option key={index} value={index}>
                  {order.name}
                </option>
              );
            })}
          </select>
        </div>
        <div className="form-group my-1">
          <label
            className="block text-gray-700 text-sm font-bold mb-2"
            htmlFor="customer"
          >
            Client
          </label>
          <select
            className="select w-full max-w-xs"
            name="customer"
            value={customers.findIndex((c) => c.id === selected.customer.id)}
            onChange={(e) => {
              setSelected({ ...selected, customer: customers[e.target.value] });
            }}
          >
            <option disabled>Client</option>
            {customers.map((customer, index) => {
              return (
                <option key={index} value={index}>
                  {customer.name}
                </option>
              );
            })}
          </select>
        </div>
        <div className="form-group my-1">
          <label
            className="block text-gray-700 text-sm font-bold mb-2"
            htmlFor="name"
          >
            Nom de la commande
          </label>
          <input
            value={selected.name}
            onChange={(e) => {
              setSelected({ ...selected, name: e.target.value });
            }}
            type="text"
            id="name"
            name="name"
            placeholder="Nom de la commande"
            className="input w-full max-w-xs border-2 border-black"
          ></input>
        </div>

        <div className="form-group my-3 text-center">
          <button
            type="submit"
            className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
          >
            Modifier la commande
          </button>
        </div>
      </form>
    </div>
  );
};
