import axios from "axios";
import { useState } from "react";
import { useRecoilState } from "recoil";
import { customersState } from "../../../Router";

export const ModifyCustomer = () => {
  // axios post request to modify a customer
  const modifyCustomer = () => {
    axios
      .put("http://localhost:8080/api/Customers/" + selected.id, selected)
      .then((response) => {
        setCustomers(
          customers.map((p) => (p.id === selected.id ? selected : p))
        );
      })
      .catch((error) => {
        console.log(error);
      });
  };

  function handleChange(e) {
    setSelectedIndex(e.target.value);
    setSelected(customers[e.target.value]);
  }

  const [customers, setCustomers] = useRecoilState(customersState);
  const [selected, setSelected] = useState(customers[0]);
  const [selectedIndex, setSelectedIndex] = useState(0);

  return customers.length === 0 ? (
    <h2 className="text-xl text-warning my-5">Aucun client à modifier 😕</h2>
  ) : (
    //a Tailwind form to modify a customer that has a name
    <div className="w-2/3 flex flex-col items-center bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4">
      <h3 className="text-2xl mb-2">Modifier un client</h3>
      <form
        className="mt-2"
        onSubmit={(e) => {
          e.preventDefault();
          modifyCustomer();
          e.target.reset();
        }}
      >
        <div className="form-group my-1">
          <label
            className="block text-gray-700 text-sm font-bold mb-2"
            htmlFor="customer"
          >
            Client à modifier
          </label>
          <select
            className="select w-full max-w-xs"
            value={selectedIndex}
            onChange={handleChange}
          >
            <option disabled>Client à modifier</option>
            {customers.map((customer, index) => {
              return (
                <option key={index} value={index}>
                  {customer.name}
                </option>
              );
            })}
          </select>
        </div>
        <div className="form-group my-1">
          <label
            className="block text-gray-700 text-sm font-bold mb-2"
            htmlFor="name"
          >
            Nom du client
          </label>
          <input
            value={selected.name}
            onChange={(e) => {
              setSelected({ ...selected, name: e.target.value });
            }}
            type="text"
            id="name"
            name="name"
            placeholder="Nom du client"
            className="input w-full max-w-xs border-2 border-black"
          ></input>
        </div>

        <div className="form-group my-3 text-center">
          <button
            type="submit"
            className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
          >
            Modifier le client
          </button>
        </div>
      </form>
    </div>
  );
};
