import axios from "axios";
import { useState } from "react";
import { useRecoilState } from "recoil";
import { productsState } from "../../../Router";

export const ModifyProduct = () => {
  // axios post request to modify a product
  const modifyProduct = () => {
    axios
      .put("http://localhost:8080/api/Products/" + selected.id, selected)
      .then((response) => {
        setProducts(products.map((p) => (p.id === selected.id ? selected : p)));
      })
      .catch((error) => {
        console.log(error);
      });
  };

  function handleChange(e) {
    setSelectedIndex(e.target.value);
    setSelected(products[e.target.value]);
  }

  const [products, setProducts] = useRecoilState(productsState);
  const [selected, setSelected] = useState(products[0]);
  const [selectedIndex, setSelectedIndex] = useState(0);

  return products.length === 0 ? (
    <h2 className="text-xl text-warning my-5">Aucun produit à modifier 😕</h2>
  ) : (
    //a Tailwind form to modify a product that has a name, a description, a price and an image url
    <div className="w-2/3 flex flex-col items-center bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4">
      <h3 className="text-2xl mb-2">Modifier un produit</h3>
      <form
        className="mt-2"
        onSubmit={(e) => {
          e.preventDefault();
          modifyProduct();
          e.target.reset();
        }}
      >
        <div className="form-group my-1">
          <label
            className="block text-gray-700 text-sm font-bold mb-2"
            htmlFor="product"
          >
            Produit à modifier
          </label>
          <select
            className="select w-full max-w-xs"
            value={selectedIndex}
            onChange={handleChange}
          >
            <option disabled>Produit à modifier</option>
            {products.map((product, index) => {
              return (
                <option key={index} value={index}>
                  {product.name}
                </option>
              );
            })}
          </select>
        </div>
        <div className="form-group my-1">
          <label
            className="block text-gray-700 text-sm font-bold mb-2"
            htmlFor="name"
          >
            Nom du produit
          </label>
          <input
            value={selected.name}
            onChange={(e) => {
              setSelected({ ...selected, name: e.target.value });
            }}
            type="text"
            id="name"
            name="name"
            placeholder="Nom du produit"
            className="input w-full max-w-xs border-2 border-black"
          ></input>
        </div>
        <div className="form-group my-1">
          <label
            className="block text-gray-700 text-sm font-bold mb-2"
            htmlFor="description"
          >
            Description
          </label>
          <input
            value={selected.description}
            onChange={(e) => {
              setSelected({ ...selected, description: e.target.value });
            }}
            type="text"
            id="description"
            name="description"
            placeholder="Description"
            className="input w-full max-w-xs border-2 border-black"
          ></input>
        </div>
        <div className="form-group my-1">
          <label
            className="block text-gray-700 text-sm font-bold mb-2"
            htmlFor="price"
          >
            Prix
          </label>
          <input
            value={selected.price}
            onChange={(e) => {
              setSelected({ ...selected, price: e.target.value });
            }}
            type="text"
            id="price"
            name="price"
            placeholder="Prix"
            className="input w-full max-w-xs border-2 border-black"
          ></input>
        </div>
        <div className="form-group my-1">
          <label
            className="block text-gray-700 text-sm font-bold mb-2"
            htmlFor="image"
          >
            URL de l'image
          </label>
          <input
            value={selected.image}
            onChange={(e) => {
              setSelected({ ...selected, image: e.target.value });
            }}
            type="text"
            id="image"
            name="image"
            placeholder="URL de l'image"
            className="input w-full max-w-xs border-2 border-black"
          ></input>
        </div>
        <div className="form-group my-3 text-center">
          <button
            type="submit"
            className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
          >
            Modifier le produit
          </button>
        </div>
      </form>
    </div>
  );
};
