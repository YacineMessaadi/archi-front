import { Navbar } from "../components/navbar/Navbar";
import { Product } from "../components/shop/Product";
import { useRecoilValue } from "recoil";
import { cartState, productsState } from "../Router";
import { Cart } from "../components/shop/Cart";

function App() {
  const products = useRecoilValue(productsState);
  const cart = useRecoilValue(cartState);

  return (
    <div className="drawer drawer-end">
      <input
        id="my-drawer-4"
        type="checkbox"
        className="drawer-toggle"
        checked={cart.open}
      />
      <div className="drawer-content">
        <div id="home">
          <Navbar />
          {products.length === 0 ? (
            <h2 className="text-2xl text-warning text-center my-5">
              Aucun produit disponible 😕
            </h2>
          ) : (
            <div id="showroom" className="grid grid-cols-3 gap-3 p-5">
              {products.map((product) => {
                return <Product product={product} />;
              })}
            </div>
          )}
        </div>
      </div>
      <Cart />
    </div>
  );
}

export default App;
