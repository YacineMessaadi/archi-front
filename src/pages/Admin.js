import { useState } from "react";
import { useRecoilValue } from "recoil";
import { Customer } from "../components/admin-cards/Customer";
import { Product } from "../components/admin-cards/Product";
import { Order } from "../components/admin-cards/Order";
import { CreateCustomer } from "../components/forms/create/CreateCustomer";
import { CreateOrder } from "../components/forms/create/CreateOrder";
import { CreateProduct } from "../components/forms/create/CreateProduct";
import { ModifyCustomer } from "../components/forms/modify/ModifyCustomer";
import { ModifyProduct } from "../components/forms/modify/ModifyProduct";
import { Navbar } from "../components/navbar/Navbar";
import { productsState } from "../Router";
import { customersState } from "../Router";
import { ordersState } from "../Router";
import { ModifyOrder } from "../components/forms/modify/ModifyOrder";

export const Admin = () => {
  const forms = [
    [
      <CreateProduct name="Produit" />,
      <CreateCustomer name="Client" />,
      <CreateOrder name="Commande" />,
    ],
    [
      <ModifyProduct name="Produit" />,
      <ModifyCustomer name="Client" />,
      <ModifyOrder name="Commande" />,
    ],
  ];

  const elements = [
    useRecoilValue(productsState),
    useRecoilValue(customersState),
    useRecoilValue(ordersState),
  ];
  const [method, setMethod] = useState(0);
  const [page, setPage] = useState(0);

  const methods = ["Créer", "Modifier"];

  return (
    <div id="home">
      <Navbar />
      <div
        id="dashboard"
        className="w-full flex flex-col items-center place-content-center"
      >
        <div className="tabs">
          {methods.map((_, index) => {
            return (
              <button
                key={index}
                className={`tab tab-bordered ${
                  method === index ? "tab-active" : ""
                }`}
                onClick={() => setMethod(index)}
              >
                {methods[index]}
              </button>
            );
          })}
        </div>
        <div className="tabs">
          {forms[method].map((_, index) => {
            return (
              <button
                key={index}
                className={`tab tab-bordered ${
                  page === index ? "tab-active" : ""
                }`}
                onClick={() => setPage(index)}
              >
                {forms[method][index].props.name}
              </button>
            );
          })}
        </div>
        {forms[method][page]}
        {elements[page].map((element, index) => {
          switch (forms[method][page].props.name) {
            case "Produit":
              return <Product key={index} product={element} />;
            case "Client":
              return <Customer key={index} customer={element} />;
            case "Commande":
              return <Order key={index} order={element} />;

            default:
              return "";
          }
        })}
      </div>
    </div>
  );
};
