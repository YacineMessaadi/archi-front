import React from "react";
import { Router } from "./Router";
import { RecoilRoot } from "recoil";
import ReactDOM from "react-dom";
import "./global.css";

ReactDOM.render(
  <RecoilRoot>
    <Router />
  </RecoilRoot>,
  document.getElementById("root")
);
