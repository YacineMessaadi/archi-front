import React, { useEffect } from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import "./global.css";
import App from "./pages/App";
import { Admin } from "./pages/Admin";
import { atom, useSetRecoilState } from "recoil";
import axios from "axios";

export const productsState = atom({
  key: "productsState",
  default: [],
});
export const ordersState = atom({
  key: "ordersState",
  default: [],
});
export const customersState = atom({
  key: "customersState",
  default: [],
});

export const cartState = atom({
  key: "cartState",
  default: { open: false, items: [] },
});

export const Router = () => {
  const setProducts = useSetRecoilState(productsState);
  const setOrders = useSetRecoilState(ordersState);
  const setCustomers = useSetRecoilState(customersState);
  const setCart = useSetRecoilState(cartState);

  useEffect(() => {
    axios
      .get("http://localhost:8080/api/Products")
      .then((response) => setProducts(response.data));
    axios
      .get("http://localhost:8080/api/Orders")
      .then((response) => setOrders(response.data));
    axios
      .get("http://localhost:8080/api/Customers")
      .then((response) => setCustomers(response.data));
    setCart(
      JSON.parse(window.localStorage.getItem("cart")) || {
        open: false,
        items: [],
      }
    );
  });

  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<App />} />
        <Route path="admin" element={<Admin />} />
      </Routes>
    </BrowserRouter>
  );
};
